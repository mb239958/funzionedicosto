/*
 *  @file test_vars_constr_cost.h
 *
 */

#include <ifopt/variable_set.h>
#include <ifopt/constraint_set.h>
#include <ifopt/cost_term.h>
#include <Eigen/Dense>
#include <iostream>

namespace ifopt
{
  using Eigen::VectorXd;

  class ExVariables : public VariableSet
  {
  public:
    // Every variable set has a name, here "var_set1". this allows the constraints
    // and costs to define values and Jacobians specifically w.r.t this variable set.
    ExVariables() : ExVariables("var_set1"){};
    ExVariables(const std::string &name) : VariableSet(6, name)
    {
      // the initial values where the NLP starts iterating from
      /*
    x0_ = 3.5;
    x1_ = 1.5;
    */

      du11 = 1;
      du12 = 1;
      du21 = 0;
      du22 = 0;
      du31 = 0;
      du32 = 0;
    }

    // Here is where you can transform the Eigen::Vector into whatever
    // internal representation of your variables you have (here two doubles, but
    // can also be complex classes such as splines, etc..

    //  void SetVariables(const VectorXd& x) override
    void SetVariables(const VectorXd &du) override
    {
      /*
    x0_ = x(0);
    x1_ = x(1);
    */
      du11 = du(0);
      du12 = du(1);
      du21 = du(2);
      du22 = du(3);
      du31 = du(4);
      du32 = du(5);
    };

    // Here is the reverse transformation from the internal representation to
    // to the Eigen::Vector
    VectorXd GetValues() const override
    {
      Eigen::VectorXd wXd(6);
      wXd << du11, du12,
          du21, du22,
          du31, du32;
      return wXd;
    };

    // Each variable has an upper and lower bound set here
    VecBound GetBounds() const override
    {
      /*
    VecBound bounds(GetRows());
    bounds.at(0) = Bounds(-1.0, 1.0);
    bounds.at(1) = NoBound;
    return bounds;
    */
      VecBound bounds(GetRows());
      bounds.at(0) = Bounds(-1.0, 1.0);
      bounds.at(1) = Bounds(-1.0, 1.0);
      bounds.at(2) = Bounds(-1.0, 1.0);
      bounds.at(3) = Bounds(-1.0, 1.0);
      bounds.at(4) = Bounds(-1.0, 1.0);
      bounds.at(5) = Bounds(-1.0, 1.0);
      return bounds;
    }

  private:
    /*
      double x0_, x1_;
    */
    double du11, du12,
           du21, du22,
           du31, du32;
  };

  class ExConstraint : public ConstraintSet
  {
  public:
    ExConstraint() : ExConstraint("constraint1") {}

    // This constraint set just contains 1 constraint, however generally
    // each set can contain multiple related constraints.
    ExConstraint(const std::string &name) : ConstraintSet(1, name) {}

    // The constraint value minus the constant value "1", moved to bounds.
    VectorXd GetValues() const override
    {
      /*
    VectorXd g(GetRows());
    Vector2d x = GetVariables()->GetComponent("var_set1")->GetValues();
    g(0) = std::pow(x(0),2) + x(1);
    return g;
    */

      // puramente fittizia sempre true per averne una
      VectorXd g(GetRows());
      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();
      g(0) = du(0) - du(0);
      return g;
    };

    // The only constraint in this set is an equality constraint to 1.
    // Constant values should always be put into GetBounds(), not GetValues().
    // For inequality constraints (<,>), use Bounds(x, inf) or Bounds(-inf, x).
    VecBound GetBounds() const override
    {
      /*  
    VecBound b(GetRows());
    b.at(0) = Bounds(1.0, 1.0);
    return b;
    */
      VecBound b(GetRows());
      b.at(0) = Bounds(0.0, 0.0);
      return b;
    }

    // This function provides the first derivative of the constraints.
    // In case this is too difficult to write, you can also tell the solvers to
    // approximate the derivatives by finite differences and not overwrite this
    // function, e.g. in ipopt.cc::use_jacobian_approximation_ = true
    void FillJacobianBlock(std::string var_set, Jacobian &jac_block) const override
    {

      // must fill only that submatrix of the overall Jacobian that relates
      // to this constraint and "var_set1". even if more constraints or variables
      // classes are added, this submatrix will always start at row 0 and column 0,
      // thereby being independent from the overall problem.

      /*
    if (var_set == "var_set1") {
      Vector2d x = GetVariables()->GetComponent("var_set1")->GetValues();

      jac_block.coeffRef(0, 0) = 2.0*x(0); // derivative of first constraint w.r.t x0
      jac_block.coeffRef(0, 1) = 1.0;      // derivative of first constraint w.r.t x1
    }
    */

      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();


        jac_block.coeffRef(0, 0) = 0.0;
        jac_block.coeffRef(0, 1) = 0.0;
        jac_block.coeffRef(0, 2) = 0.0;
        jac_block.coeffRef(0, 3) = 0.0;
        jac_block.coeffRef(0, 4) = 0.0;
        jac_block.coeffRef(0, 5) = 0.0;
      }
    }
  };


double pi = 3.14159265358;




    //-------------------------------------------------------------------------------
    //  recuperate da funzione_di_costo - traduzione cpp di matlab
    //-------------------------------------------------------------------------------
    // --------------------------------------------------------------------
    Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
    {
      //Continuous-time nonlinear model of a differential drive mobile robot

      // 3 states (x):
      //   robot x position (x)
      //   robot y position (y)
      //  robot yaw angle (theta)
      //2 inputs (u):
      //linear velocity (v_r)
      //angular velocity(w_r)
      double theta_r = x(2);
      Eigen::VectorXd dxdt = x;
      double v_r = u(0);
      double w_r = u(1);
      dxdt(0) = cos(theta_r) * v_r;
      dxdt(1) = sin(theta_r) * v_r;
      dxdt(2) = w_r;
      Eigen::VectorXd y = x;

      return dxdt;
    };

    // --------------------------------------------------------------------
    Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
    {
      int M = 10;
      double delta = Ts / M;
      Eigen::VectorXd xk1 = xk;
      for (int i = 0; i < M; i++)
      {
        
        
      }
      //wraptopi
      while (xk1(2) > 2 * pi || xk1(2) < (-2 * pi))
      {
        if (xk1(2) > 0)
        {
          xk1(2) = xk1(2) - 2 * pi;
        }
        else
        {
          xk1(2) = xk1(2) + 2 * pi;
        }
      }
      if (xk1(2) > pi)
      {
        xk1(2) = xk1(2) - pi;
        xk1(2) = xk1(2) - pi;
      }
      if (xk1(2) < -pi)
      {
        xk1(2) = xk1(2) + pi;
        xk1(2) = xk1(2) + pi;
      }
      Eigen::VectorXd yk = xk;

      return xk1;
    };

    // --------------------------------------------------------------------
    double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
    {
      /* Cost function of nonlinear MPC for mobile robot control

 Inputs:
   du:      optimization variable, from time k to time k+M-1 as a column vector
   x:      current state at time k (now)
   Ts:     controller sample time
   N:      prediction horizon
   M:      control horizon
   xref:   state references as a matrix whose N+1(from instant 0 to N) rows are the reference for the system

   u1:     previous controller output at time k-1
   J:      objective function cost

 NonlinearTs MPC design parameters
 */

      //Q matrix penalizes state deviations from the references.
      Eigen::DiagonalMatrix<double, 3> Q(8, 10, 0.1);

      Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

      // Rdu matrix penalizes the input rate of change (du).
      Eigen::DiagonalMatrix<double, 2> Rdu(0.005, 0.005);

      // Ru matrix penalizes the input magnitude
      Eigen::DiagonalMatrix<double, 2> Ru(0.005, 0.005);

      //init ref_k
      Eigen::VectorXd ref_k(3);
      ref_k << 0, 0, 0;

      //init theta_d
      double theta_d = 0.0;

      //init ek
      Eigen::VectorXd ek(3);
      ek << 0, 0, 0;

      // reshape du to a matrix which rows are du[k] from k to k+N, filling
      // rows after M with zeros
      Eigen::MatrixXd alldu(N, 2);
      alldu.setZero(N, 2);
      int w = 0;
      for (int i = 0; i < M; ++i)
      {
        for (int c = 0; c < 2; ++c)
        {
          alldu(i, c) = du(w);
          w++;
        }
      }

      /*
  Cost Calculation
  Set initial plant states and cost.
  */

      //xk = x0;
      Eigen::VectorXd xk(3);
      xk << 0, 0, 0;
      for (int i = 0; i < 3; i++)
      {
        xk(i) = x0(i);
      }

      //uk = u1;
      Eigen::VectorXd uk(2);
      uk << 0, 0;
      for (int i = 0; i < 2; i++)
      {
        uk(i) = u1(i);
      }

      //init J
      double j = 0;

      for (int i = 0; i < N; i++)
      {
        Eigen::VectorXd du_k(2);
        du_k << 0, 0;
        for (int j = 0; j < 2; j++)
        {
          du_k(j) = alldu(i, j);
        }
        du_k = du_k.transpose();
        uk = uk + du_k;

        for (int j = 0; j < 3; j++)
        {
          ref_k(j) = xref(i, j);
        }
        ref_k = ref_k.transpose();
        theta_d = ref_k(2);

        //Rot
        Eigen::MatrixXd Rot(3, 3);
        Rot.setZero(3, 3);

        Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

        ek = Rot * (xk - ref_k);

        j = j + ek.transpose() * Q * ek;
        j = j + du_k.transpose() * Rdu * du_k;
        j = j + uk.transpose() * Ru * uk;

        //obtain plant state at next prediction step
        xk = mobRobDT0(xk, uk, Ts);
      }
      //terminal cost
      for (int j = 0; j < 3; j++)
      {
        ref_k(j) = xref(N, j);
      }

      ref_k = ref_k.transpose();

      theta_d = ref_k(2);

      Eigen::MatrixXd Rot(3, 3);
      Rot.setZero(3, 3);

      Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
      ek = Rot * (xk - ref_k);

      j = j + ek.transpose() * P * ek;

      //output J
      //std::cout << "J = " <<std::to_string(j) << std::endl;

      return j;
    };

    double FunzioneDiCosto(Eigen::VectorXd du)
    {

      int M = 3;
      int N = 5;

      //Eigen::VectorXd du(6);
      //du << 1, 1, 0, 0, 0, 0;

      Eigen::VectorXd x0(3);
      x0 << 1, 1, pi / 2;

      double Ts = 0.1;

      Eigen::MatrixXd xref(N + 1, 3);
      xref.setZero(N + 1, 3);
      Eigen::VectorXd u1(2);
      u1 << 0, 0;

      double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
      return j;
    };


    // --------------------------------------------------------------------




  class ExCost : public CostTerm
  {
  public:
    ExCost() : ExCost("cost_term1") {}
    ExCost(const std::string &name) : CostTerm(name) {}

    double GetCost() const override
    {
      /*
    Vector2d x = GetVariables()->GetComponent("var_set1")->GetValues();
    return -std::pow(x(1)-2,2);
  */
      VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();
      double j = FunzioneDiCosto(du);
      return j;
    };

    void FillJacobianBlock(std::string var_set, Jacobian &jac) const override
    {

      /*
    if (var_set == "var_set1") {
      Vector2d x = GetVariables()->GetComponent("var_set1")->GetValues();

      jac.coeffRef(0, 0) = 0.0;             // derivative of cost w.r.t x0
      jac.coeffRef(0, 1) = -2.0*(x(1)-2.0); // derivative of cost w.r.t x1
    }
    */

      if (var_set == "var_set1")
      {
        VectorXd du = GetVariables()->GetComponent("var_set1")->GetValues();

        Eigen::VectorXd x0(3);
        x0 << 1, 1, pi / 2;
        Eigen::VectorXd h(6);
        h << 0.001, 0.001, 0.001,0.001,0.001,0.001; 

        /*double dj_su_du_11=0;
        double dj_su_du_12=0;
        double dj_su_du_21=0;
        double dj_su_du_22=0;
        double dj_su_du_31=0;
        double dj_su_du_32=0;*/
        
        double wdx1,wdy1,wdx2,wdy2,wdx3,wdy3,wdx4,wdy4,wdx5,wdy5 = 0;
        double vdx1,vdy1,vdx2,vdy2,vdx3,vdy3,vdx4,vdy4,vdx5,vdy5 = 0;

        wdx1 = (cos(x0(2)) + cos(x0(2) + 0.01*du(1) ) + cos(x0(2) + 0.02*du(1) ) +cos(x0(2) + 0.03*du(1) ) +cos(x0(2) + 0.04*du(1) ) +cos(x0(2) + 0.05*du(1) ) +cos(x0(2) + 0.06*du(1) ) +cos(x0(2) + 0.07*du(1) ) +cos(x0(2) + 0.08*du(1) ) +cos(x0(2) + 0.09*du(1) ));
        wdy1 = (sin(x0(2)) + sin(x0(2) + 0.01*du(1) ) + sin(x0(2) + 0.02*du(1) ) +sin(x0(2) + 0.03*du(1) ) +sin(x0(2) + 0.04*du(1) ) +sin(x0(2) + 0.05*du(1) ) +sin(x0(2) + 0.06*du(1) ) +sin(x0(2) + 0.07*du(1) ) +sin(x0(2) + 0.08*du(1) ) +sin(x0(2) + 0.09*du(1) ));
        wdx2 = (cos(x0(2)) + cos(x0(2) + 0.1*du(1) + 0.01*(du(1) + du(3)) ) + cos(x0(2)+ 0.1*du(1)+ 0.02*(du(1) + du(3)) ) +cos(x0(2)+ 0.1*du(1) + 0.03*(du(1) + du(3)) ) +cos(x0(2) + 0.1*du(1)+ 0.04*(du(1) + du(3)) ) +cos(x0(2) + 0.1*du(1)+ 0.05*(du(1) + du(3)) ) +cos(x0(2) + 0.1*du(1)+ 0.06*(du(1) + du(3)) ) +cos(x0(2) + 0.1*du(1)+ 0.07*(du(1) + du(3)) ) +cos(x0(2)+ 0.1*du(1) + 0.08*(du(1) + du(3)) ) +cos(x0(2) + 0.1*du(1)+ 0.09*(du(1) + du(3)) ));
        wdy2 = (sin(x0(2)) + sin(x0(2) + 0.1*du(1) + 0.01*(du(1) + du(3)) ) + sin(x0(2)+ 0.1*du(1)+ 0.02*(du(1) + du(3)) ) +sin(x0(2)+ 0.1*du(1) + 0.03*(du(1) + du(3)) ) +sin(x0(2) + 0.1*du(1)+ 0.04*(du(1) + du(3)) ) +sin(x0(2) + 0.1*du(1)+ 0.05*(du(1) + du(3)) ) +sin(x0(2) + 0.1*du(1)+ 0.06*(du(1) + du(3)) ) +sin(x0(2) + 0.1*du(1)+ 0.07*(du(1) + du(3)) ) +sin(x0(2)+ 0.1*du(1) + 0.08*(du(1) + du(3)) ) +sin(x0(2) + 0.1*du(1)+ 0.09*(du(1) + du(3)) ));
        wdx3 = (cos(x0(2)) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ));
        wdy3 = (sin(x0(2)) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ));
        wdx4 = (cos(x0(2)) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.10*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.11*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 12*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.13*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.14*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.15*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.16*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.17*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.18*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.19*(du(1) + du(3) + du(5)) ));
        wdy4 = (sin(x0(2)) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.10*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.11*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 12*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.13*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.14*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.15*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.16*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.17*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.18*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.19*(du(1) + du(3) + du(5)) ));
        wdx5 = (cos(x0(2)) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +cos(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.10*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.11*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 12*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.13*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.14*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.15*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.16*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.17*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.18*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.19*(du(1) + du(3) + du(5)) ) + cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.20*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.21*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.22*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.23*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.24*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.25*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.26*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.27*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.28*(du(1) + du(3) + du(5)) )+ cos(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.29*(du(1) + du(3) + du(5)) ));
        wdy5 = (sin(x0(2)) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.01*(du(1) + du(3) + du(5)) ) + sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.02*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.03*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.04*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.05*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.06*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.07*(du(1) + du(3) + du(5)) ) +sin(x0(2)+ 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.08*(du(1) + du(3) + du(5)) ) +sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3))+ 0.09*(du(1) + du(3) + du(5)) ) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.10*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.11*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 12*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.13*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.14*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.15*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.16*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.17*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.18*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.19*(du(1) + du(3) + du(5)) ) + sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.20*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.21*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.22*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.23*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.24*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.25*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.26*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.27*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.28*(du(1) + du(3) + du(5)) )+ sin(x0(2) + 0.1*du(1)+ 0.1*(du(1) + du(3)) + 0.29*(du(1) + du(3) + du(5)) ));
        
       /* vdx1 = ( 0.01*sin(x0(2) + 0.01*du(1) ) + 0.02*sin(x0(2) + 0.02*du(1) ) +0.03*sin(x0(2) + 0.03*du(1) ) +0.04*sin(x0(2) + 0.04*du(1) ) +0.05*sin(x0(2) + 0.05*du(1) ) +0.06*sin(x0(2) + 0.06*du(1) ) +0.07*sin(x0(2) + 0.07*du(1) ) +0.08*sin(x0(2) + 0.08*du(1) ) +0.09*sin(x0(2) + 0.09*du(1) ));
        vdy1 = ( 0.01*cos(x0(2) + 0.01*du(1) ) + 0.02*cos(x0(2) + 0.02*du(1) ) +0.03*cos(x0(2) + 0.03*du(1) ) +0.04*cos(x0(2) + 0.04*du(1) ) +0.05*cos(x0(2) + 0.05*du(1) ) +0.06*cos(x0(2) + 0.06*du(1) ) +0.07*cos(x0(2) + 0.07*du(1) ) +0.08*cos(x0(2) + 0.08*du(1) ) +0.09*cos(x0(2) + 0.09*du(1) ));
        vdx2 = ( 0.1*sin(x0(2) + 0.1*du(1) )+0.1*sin(x0(2) + 0.1*du(1)+ 0.01*(du(3)+du(1)) ) + 0.1*sin(x0(2)  + 0.1*du(1)+ 0.02*(du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1) + 0.03*(du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.04*(du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1) + 0.05*(du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1) + 0.06*(du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.07*(du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1) + 0.08*(du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1) + 0.09*(du(3)+du(1)) ));
        vdy2 = ( 0.1*cos(x0(2) + 0.1*du(1) )+0.1*cos(x0(2) + 0.1*du(1)+ 0.01*(du(3)+du(1)) ) + 0.1*cos(x0(2)  + 0.1*du(1)+ 0.02*(du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1) + 0.03*(du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.04*(du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1) + 0.05*(du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1) + 0.06*(du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.07*(du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1) + 0.08*(du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1) + 0.09*(du(3)+du(1)) ));
        vdx3 = ( 0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) )+0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.01*(du(5)+du(3)+du(1)) ) + 0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.02*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.03*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.04*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.05*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.06*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.07*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.08*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.09*(du(5)+du(3)+du(1)) ));
        vdy3 = ( 0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) )+0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.01*(du(5)+du(3)+du(1)) ) + 0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.02*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.03*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.04*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.05*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.06*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.07*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.08*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.09*(du(5)+du(3)+du(1)) ));
        vdx4 = ( 0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.10*(du(5)+du(3)+du(1)) ) + 0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.11*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.12*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.13*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.14*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.15*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.16*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.17*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.18*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.19*(du(5)+du(3)+du(1)) ));
        vdy4 = ( 0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.10*(du(5)+du(3)+du(1)) ) + 0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.11*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.12*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.13*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.14*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.15*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.16*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.17*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.18*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.19*(du(5)+du(3)+du(1)) ));
        vdx5 = ( 0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.20*(du(5)+du(3)+du(1)) ) + 0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.21*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.22*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.23*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.24*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.25*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.26*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.27*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.28*(du(5)+du(3)+du(1)) ) +0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.29*(du(5)+du(3)+du(1)) ));
        vdy5 = ( 0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.20*(du(5)+du(3)+du(1)) ) + 0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.21*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.22*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.23*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.24*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.25*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.26*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.27*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.28*(du(5)+du(3)+du(1)) ) +0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.29*(du(5)+du(3)+du(1)) ));
       

        dj_su_du_11 = 0.005 * (2*du(0) - 2*du(2) + 2*du(0)) + 
                      16*(x0(0) + 0.01*wdx1 *du(0))  *(0.01*wdx1) +  
                      20*(x0(1) + 0.01*wdy1 *du(0))  *(0.01*wdy1) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0)) ) *(0.01*wdx1) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0)) ) *(0.01*wdy1) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0)) )  *(0.01*wdx1) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0)) )  *(0.01*wdy1) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0)))  *(0.01*wdx1) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0)))  *(0.01*wdy1) + 
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)) )  *(0.01*wdx1) +  
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))  *(0.01*wdy1)  ;
        dj_su_du_12 = 0.005*(2*du(1)-2*du(3)+2*du(1)) +
                      16*(x0(0) + 0.01*wdx1 *du(0))*(-0.01*du(0)*vdx1) +
                      20*(x0(1) + 0.01*wdy1 *du(0))*(0.01*du(0)*vdy1)  + 
                      0.02*(x0(2) + 0.1*du(1)) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0)))*(-0.01*du(0)*vdx1-0.01*(du(2)+ du(0))*vdx2) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0)))*(0.01*du(0)*vdy1 +0.01*(du(2)+ du(0))*vdy2) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))) +  
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0)))*(-0.01*du(0)*vdx1-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0)))*(0.01*du(0)*vdy1 +0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.1*(du(5)+du(3)+du(1))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(-0.01*du(0)*vdx1-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(0.01*du(0)*vdy1 +0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.2*(du(5)+du(3)+du(1))) +
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)))*(-0.01*du(0)*vdx1-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4-0.01*(du(4)+du(2)+ du(0))*vdx5) +
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))*(0.01*du(0)*vdy1 +0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4+0.01*(du(4)+du(2)+ du(0))*vdy5) + 
                      2*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.3*(du(5)+du(3)+du(1)));
        dj_su_du_21 = 0.005 * (2*du(2) - 2*du(4) + 2*(du(2)+du(0))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0)) ) *(0.01*wdx2) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0)) ) *(0.01*wdy2) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0)) )  *(0.01*wdx2) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0)) )  *(0.01*wdy2) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0)))  *(0.01*wdx2) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0)))  *(0.01*wdy2) + 
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)) )  *(0.01*wdx2) +  
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))  *(0.01*wdy2)  ;
        vdx2=( 0.01*sin(x0(2) + 0.1*du(1)+ 0.01*(du(3)+du(1)) ) + 0.02*sin(x0(2)  + 0.1*du(1)+ 0.02*(du(3)+du(1)) ) +0.03*sin(x0(2) + 0.1*du(1) + 0.03*(du(3)+du(1)) ) +0.04*sin(x0(2)  + 0.1*du(1)+ 0.04*(du(3)+du(1)) ) +0.05*sin(x0(2) + 0.1*du(1) + 0.05*(du(3)+du(1)) ) +0.06*sin(x0(2) + 0.1*du(1) + 0.06*(du(3)+du(1)) ) +0.07*sin(x0(2)  + 0.1*du(1)+ 0.07*(du(3)+du(1)) ) +0.08*sin(x0(2) + 0.1*du(1) + 0.08*(du(3)+du(1)) ) +0.09*sin(x0(2) + 0.1*du(1) + 0.09*(du(3)+du(1)) ));
        vdy2=( 0.01*cos(x0(2) + 0.1*du(1)+ 0.01*(du(3)+du(1)) ) + 0.02*cos(x0(2)  + 0.1*du(1)+ 0.02*(du(3)+du(1)) ) +0.03*cos(x0(2) + 0.1*du(1) + 0.03*(du(3)+du(1)) ) +0.04*cos(x0(2)  + 0.1*du(1)+ 0.04*(du(3)+du(1)) ) +0.05*cos(x0(2) + 0.1*du(1) + 0.05*(du(3)+du(1)) ) +0.06*cos(x0(2) + 0.1*du(1) + 0.06*(du(3)+du(1)) ) +0.07*cos(x0(2)  + 0.1*du(1)+ 0.07*(du(3)+du(1)) ) +0.08*cos(x0(2) + 0.1*du(1) + 0.08*(du(3)+du(1)) ) +0.09*cos(x0(2) + 0.1*du(1) + 0.09*(du(3)+du(1)) ));
        dj_su_du_22 = 0.005*(2*du(3)-2*du(5)+2*(du(3)+du(1))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0)))*(-0.01*(du(2)+ du(0))*vdx2) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0)))*(0.01*(du(2)+ du(0))*vdy2) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))) +  
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0)))*(-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0)))*(0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.1*(du(5)+du(3)+du(1))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.2*(du(5)+du(3)+du(1))) +
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)))*(-0.01*(du(2)+ du(0))*vdx2-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4-0.01*(du(4)+du(2)+ du(0))*vdx5) +
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))*(0.01*(du(2)+ du(0))*vdy2+0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4+0.01*(du(4)+du(2)+ du(0))*vdy5) + 
                      2*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.3*(du(5)+du(3)+du(1)));
        dj_su_du_31 = 0.005 * (2*du(4)  + 6*(du(4)+du(2)+du(0))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0)) )  *(0.01*wdx3) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0)) )  *(0.01*wdy3) + 
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0)))  *(0.01*wdx3+0.01*wdx4) +  
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0)))  *(0.01*wdy3+0.01*wdy4) + 
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))  +0.01*wdx3*(du(4) + du(2)+ du(0))  +0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)) )  *(0.01*wdx3+0.01*wdx4+0.01*wdx5) +  
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))  +0.01*wdy3*(du(4) + du(2)+ du(0))  +0.01*wdy4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))  *(0.01*wdy3+0.01*wdy4+0.01*wdy5)  ;          
        vdx3 = (0.01*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.01*(du(5)+du(3)+du(1)) ) + 0.02*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.02*(du(5)+du(3)+du(1)) ) +0.03*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.03*(du(5)+du(3)+du(1)) ) +0.04*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.04*(du(5)+du(3)+du(1)) ) +0.05*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.05*(du(5)+du(3)+du(1)) ) +0.06*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.06*(du(5)+du(3)+du(1)) ) +0.07*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.07*(du(5)+du(3)+du(1)) ) +0.08*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.08*(du(5)+du(3)+du(1)) ) +0.09*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.09*(du(5)+du(3)+du(1)) ));
        vdy3 = (0.01*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.01*(du(5)+du(3)+du(1)) ) + 0.02*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.02*(du(5)+du(3)+du(1)) ) +0.03*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.03*(du(5)+du(3)+du(1)) ) +0.04*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.04*(du(5)+du(3)+du(1)) ) +0.05*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.05*(du(5)+du(3)+du(1)) ) +0.06*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.06*(du(5)+du(3)+du(1)) ) +0.07*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.07*(du(5)+du(3)+du(1)) ) +0.08*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.08*(du(5)+du(3)+du(1)) ) +0.09*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.09*(du(5)+du(3)+du(1)) ));
        vdx4 = ( 0.1*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.10*(du(5)+du(3)+du(1)) ) + 0.11*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.11*(du(5)+du(3)+du(1)) ) +0.12*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.12*(du(5)+du(3)+du(1)) ) +0.13*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.13*(du(5)+du(3)+du(1)) ) +0.14*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.14*(du(5)+du(3)+du(1)) ) +0.15*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.15*(du(5)+du(3)+du(1)) ) +0.16*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.16*(du(5)+du(3)+du(1)) ) +0.17*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.17*(du(5)+du(3)+du(1)) ) +0.18*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.18*(du(5)+du(3)+du(1)) ) +0.19*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.19*(du(5)+du(3)+du(1)) ));
        vdy4 = ( 0.1*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.10*(du(5)+du(3)+du(1)) ) + 0.12*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.11*(du(5)+du(3)+du(1)) ) +0.12*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.12*(du(5)+du(3)+du(1)) ) +0.13*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.13*(du(5)+du(3)+du(1)) ) +0.14*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.14*(du(5)+du(3)+du(1)) ) +0.15*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.15*(du(5)+du(3)+du(1)) ) +0.16*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.16*(du(5)+du(3)+du(1)) ) +0.17*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.17*(du(5)+du(3)+du(1)) ) +0.18*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.18*(du(5)+du(3)+du(1)) ) +0.19*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.19*(du(5)+du(3)+du(1)) ));
        vdx5 = ( 0.20*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.20*(du(5)+du(3)+du(1)) ) + 0.21*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.21*(du(5)+du(3)+du(1)) ) +0.22*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.22*(du(5)+du(3)+du(1)) ) +0.23*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.23*(du(5)+du(3)+du(1)) ) +0.24*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.24*(du(5)+du(3)+du(1)) ) +0.25*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.25*(du(5)+du(3)+du(1)) ) +0.26*sin(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.26*(du(5)+du(3)+du(1)) ) +0.27*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.27*(du(5)+du(3)+du(1)) ) +0.28*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.28*(du(5)+du(3)+du(1)) ) +0.29*sin(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.29*(du(5)+du(3)+du(1)) ));
        vdy5 = ( 0.20*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.20*(du(5)+du(3)+du(1)) ) + 0.21*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.21*(du(5)+du(3)+du(1)) ) +0.22*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.22*(du(5)+du(3)+du(1)) ) +0.23*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.23*(du(5)+du(3)+du(1)) ) +0.24*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.24*(du(5)+du(3)+du(1)) ) +0.25*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.25*(du(5)+du(3)+du(1)) ) +0.26*cos(x0(2)  + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.26*(du(5)+du(3)+du(1)) ) +0.27*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.27*(du(5)+du(3)+du(1)) ) +0.28*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.28*(du(5)+du(3)+du(1)) ) +0.29*cos(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1)) + 0.29*(du(5)+du(3)+du(1)) ));
       
        dj_su_du_32 = 0.005*(2*du(5)+6*(du(5)+du(3)+du(1))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0)))*(-0.01*(du(4)+du(2)+ du(0))*vdx3) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0)))*(0.01*(du(4)+du(2)+ du(0))*vdy3) + 
                      0.02*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.1*(du(5)+du(3)+du(1))) +
                      16*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4) +
                      20*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0)))*(0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4) + 
                      0.04*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.2*(du(5)+du(3)+du(1))) +
                      1600*(x0(0) + 0.01*wdx1 *du(0) +0.01*wdx2*(du(2)+ du(0))+0.01*wdx3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdx5*(du(4) + du(2)+ du(0)))*(-0.01*(du(4)+du(2)+ du(0))*vdx3-0.01*(du(4)+du(2)+ du(0))*vdx4-0.01*(du(4)+du(2)+ du(0))*vdx5) +
                      2000*(x0(1) + 0.01*wdy1 *du(0) +0.01*wdy2*(du(2)+ du(0))+0.01*wdy3*(du(4)+du(2)+ du(0))+0.01*wdx4*(du(4) + du(2)+ du(0))+0.01*wdy5*(du(4) + du(2)+ du(0)))*(0.01*(du(4)+du(2)+ du(0))*vdy3+0.01*(du(4)+du(2)+ du(0))*vdy4+0.01*(du(4)+du(2)+ du(0))*vdy5) + 
                      6*(x0(2) + 0.1*du(1)+ 0.1*(du(3)+du(1))+ 0.3*(du(5)+du(3)+du(1)));
                      

*/       std::cout << "--------------------------------------"<< std::endl;
         double FC = FunzioneDiCosto(du);
         du(0)= du(0) + h(0);
         double FC_h = FunzioneDiCosto(du);
         std::cout << "J = " <<std::to_string(FC) << std::endl;
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u11= (FC_h-FC)/0.001;

         du(0)= du(0) - h(0);
         du(1)= du(1) + h(1);
         FC_h = FunzioneDiCosto(du);
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u12= (FC_h-FC)/0.001;

         du(1)= du(1) - h(1);
         du(2)= du(2) + h(2);
         FC_h = FunzioneDiCosto(du);
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
        
         double derivata_approssimata_u21= (FC_h-FC)/0.001;

         du(2)= du(2) - h(2);
         du(3)= du(3) + h(3);
         FC_h = FunzioneDiCosto(du);
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u22= (FC_h-FC)/0.001;

         du(3)= du(3) - h(3);
         du(4)= du(4) + h(4);
         FC_h = FunzioneDiCosto(du);
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u31= (FC_h-FC)/0.001;

         du(4)= du(4) - h(4);
         du(5)= du(5) + h(5);
         FC_h = FunzioneDiCosto(du);
         du(5)= du(5) - h(5);
         std::cout << "J_h = " <<std::to_string(FC_h) << std::endl;
         double derivata_approssimata_u32= (FC_h-FC)/0.001;
         std::cout << "--------------------------------------"<< std::endl;
         std::cout <<du<<std::endl;

        jac.coeffRef(0, 0) = derivata_approssimata_u11;
        jac.coeffRef(0, 1) = derivata_approssimata_u12;
        jac.coeffRef(0, 2) = 1/(1/(derivata_approssimata_u21)-1/(derivata_approssimata_u11));
        jac.coeffRef(0, 3) = 1/(1/(derivata_approssimata_u22)-1/(derivata_approssimata_u12));
        jac.coeffRef(0, 4) = 1/(1/(derivata_approssimata_u31)-1/(derivata_approssimata_u21));
        jac.coeffRef(0, 5) = 1/(1/(derivata_approssimata_u32)-1/(derivata_approssimata_u22));
       /* jac.coeffRef(0, 0) = dj_su_du_11;
        jac.coeffRef(0, 1) = dj_su_du_12;
        jac.coeffRef(0, 2) = 1/(1/(dj_su_du_21)-1/(dj_su_du_11));
        jac.coeffRef(0, 3) = 1/(1/(dj_su_du_22)-1/(dj_su_du_12));
        jac.coeffRef(0, 4) = 1/(1/(dj_su_du_31)-1/(dj_su_du_21));
        jac.coeffRef(0, 5) = 1/(1/(dj_su_du_32)-1/(dj_su_du_22));*/

      }
    };

  };

} // namespace ifopt
